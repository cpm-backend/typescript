const add = (a: number, b: number): number => {
  return a + b;
};

const substract = (a: number, b: number) => {
  return a - b;
};

function divide(a: number, b: number): number {
  return a / b;
}

const multiply = function (a: number, b: number): number {
  return a * b;
};

// If we don't add type for return, TS will infer this case is no return, but the truth is we forgot to add return statement, it should return number instead of void.
const fakeAdd = (a: number, b: number) => {
  a + b;
};

// You can also return null or undefined, but we usally don't give this function return statement
const logger = (message: string): void => {
  console.log(message);
};

const throwError1 = (message: string): string => {
  if (!message) {
    throw new Error(message);
  }
  return message;
};

// In this special case, we does not always get any return. You can also use 'void' type
const throwError2 = (message: string): never => {
  throw new Error(message);
};

// Destructing with annotations
const todaysWeather = {
  date: new Date(),
  weather: 'sunny',
};

const logWeather = ({
  date,
  weather,
}: {
  date: Date;
  weather: string;
}): void => {
  console.log(date);
  console.log(weather);
};

logWeather(todaysWeather);
