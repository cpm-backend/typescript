# Typescript Basics

*Typescript* is equal to *Javascript* plus a type system. This type system has some features:
- Helps us catch error during development. 
- Uses *type annotation* to analyze our code.
- It's only active during development.
- Does not provide any performance optimization.

We cannot execute *Typescript* directly (in browser or *Node*). Instead we have to use complier to translate *Typescript* to *Javascript*. Then we run *Javascript* code.
<br/><img src="./screenshots/typescript.png" width="500"/><br/>

Writing Typescript is the same as writing Javascript with some *extra documentation*. *Typescript* also has no effect on how our code gets executed by the browser or *Node*.

First thing to do is to install Typescript complier.
```
npm install -g typescript ts-node
```

It's recommended to write *Typescript* by *VSCode*. After installing *VSCode*, do following settings:
1. Add `code` to *PATH*: Type `shift + command + P` and search **Shell Command: Install 'code' command in PATH**, then click it to install. Now you can use `code .` to open folder by *VSCode* from terminal.
    <br/><img src="./screenshots/install-code.png" width="300"/>
1. Install code formatter *Prettier Extension*: Type `shift + command + X` and search **Prettier - Code formatter**. Then click it to install.
    <br/><img src="./screenshots/prettier.png" width="700"/>
1. (Optional) If you want to run *Prettier* on save, type `command + ,` and search **format on save** then check the box.
    <br/><img src="./screenshots/format-on-save.png" width="500"/>
1. (Optional) If you'd like to use single quotes with *Prettier*, type `command + ,` and search **single quotes** then check the following box.
    <br/><img src="./screenshots/single-quote.png" width="300"/>
1. (Optional) If you'd like to use 2 spaces for indentation, type `command + ,` and search **tab size** then type `2` in box.
1. (Optional) If you want to change theme, type `command + ,` and search **theme** then choose any theme you like, e.g. *Solarized Light*.
    <br/><img src="./screenshots/theme.png" width="350"/>

## Get Started
We are going to build a simple app to get a taste of *Typescript* (TS) basics. In this app, we will fetch the data from [JSONPlaceholder](https://jsonplaceholder.typicode.com/). Do the following steps:
1. Create a project [fetchjson](fetchjson/):
    ```
    mkdir fetchjson
    cd fetchjson
    npm init -y
    ```
1. Install `axios` package.
    ```
    npm install axios
    ```
1. Create a file [index.ts](fetchjson/index.ts) (It's `.ts` instead of `.js`).
    ```ts
    import axios from 'axios';

    const url = 'https://jsonplaceholder.typicode.com/todos/1';
    axios.get(url).then((response) => {
      console.log(response.data);
    });
    ```
1. Compile this file then you should get a file [index.js](fetchjson/index.js).
    ```
    tsc index.ts
    ```
1. Run JS file to see the result.
    ```
    node index.js
    ```
1. You can use `ts-node` to combine `tsc` and `node` command together. Run following command then you will see the same result.
    ```
    ts-node index.ts
    ```

Now if we want to retrieve the each property of response object, it's error-prone and we need to find the error while executing. That's why we need TS, we can find this kind of error in before executing. To define the structure if an object, we can use `interface`. Update your `index.ts` like so:

```ts
interface Todo {
  id: number;
  title: string;
  completed: boolean;
}

axios.get(url).then((response) => {
  const todo = response.data as Todo;
  const ID = todo.ID;
  const title = todo.Title;
  const completed = todo.completed;
  console.log(`
    The Todo with ID: ${ID}
    Has a title of: ${title}
    Is it finished? ${completed}
  `);
});
```

You will see error annotation like so:
<br/><img src="./screenshots/compile-error.png" width="600"/><br/>

## Type Annotations

Types in TS are just shortcuts, they are labels. It's an easy way to refer to the different properties and functions that a single value has. Every value in TS has a type. For example:

Type|Values that have this type
--|--
string|'hi there', ""
number|.00025, -20, 40000
boolean|true, false
Date|new Date()
Todo|{id: 1, completed: true, title: "Trash"}

There are 2 different categories of types.

- *Primitive Types* (basic types): number, boolean, string, void, undefined, symbol, null.
- *Object Types* (user can define): functions, classes, arrays, objects.

Types are used by the *Typescript compiler* to analyze our code for errors. Types also allow other engineers to understand what values are flowing around our codebase.

### Type Annotations for Variables
*Type annotations* is code we add to tell TS what types of value a variable will refer to. Another similar feature is *type inference*, which TS tries to figure out what type of value a variable refers to. To sum up, *type annotations* is we tell TS the type, *type inference* is TS guesses the type.

Here are some examples of syntax for variables (see [variables.ts](features/annotations/variables.ts)):
```ts
let apples: number = 5;
let speed: string = 'fast';
let hasName: boolean = true;

let nothingMuch: null = null;
let nothing: undefined = undefined;

// Built-in objects
let now: Date = new Date();

// Array
let colors: string[] = ['red', 'green', 'blue'];
let myNumbers: number[] = [1, 2, 3];
let truths: boolean[] = [true, true, false];

// Classes
class Car {}

let car: Car = new Car();

// Object literal
let point: { x: number; y: number } = {
  x: 10,
  y: 20,
};

// Function: actual function is after '='
const logNumber: (i: number) => void = (i: number) => {
  console.log(i);
};
```

If declaration and initialization are one the same line (one single expression), TS will figure out the type for us. This is so-called *type inference*. For example:
``` ts
// Type inference works, TS knows the type
let firstNumber = 1;

// Type inference doesn't work, TS doesn't know the type
let secondNumber;
secondNumber = 2;
```

*Type inference* will guess the type automatically, we should always use it. The important question is when to use *type annotation*?
- When a function returns the `any` type and we need to clarify the value. e.g. `parse` can return a value with different type, so the type of return value is `any`.
    ```ts
    const json = '{"x": 10, "y": 20}';
    const coordinates: { x: number; y: number } = JSON.parse(json);
    console.log(coordinates); // {x: 10, y: 20};
    ```
    > ### *What's `any` type?*
    > `any` type is a type, which means TS has no idea what this type is. It cannot check for correct property references. **We should avoid variables with `any` at all costs.**
- When we declare a variable on one line then initialize it later. See the following example, but remember initializing the variable is a good practice.
    ```ts
    let words = ['red', 'green', 'blue'];

    // Initialize 'foundWord' as false might be better in practice
    let foundWord: boolean;

    for (let i = 0; i < words.length; i++) {
      if (words[i] === 'green') {
        foundWord = true;
      }
    }
    ```
- When we want a variable to have a type that can't be inferred. e.g. we want the type of `numberAboveZero` is boolean or number.
    ```ts
    let numbers = [-10, -1, 12];
    let numberAboveZero: boolean | number = false; // one of two types

    for (let i = 0; i < numbers.length; i++) {
      if (numbers[i] > 0) {
        numberAboveZero = numbers[i];
      }
    }
    ```

### Type Annotations for Functions
Type annotations for functions is code we add to tell TS what type of arguments a function will receive and what type of values it will return. Type inference for functions, which means TS tries to figure out what type of value a function will return.

See some examples in [functions.ts](features/annotations/functions.ts):

```ts
const add = (a: number, b: number): number => {
  return a + b;
};

const substract = (a: number, b: number) => {
  return a - b;
};

function divide(a: number, b: number): number {
  return a / b;
}

const multiply = function (a: number, b: number): number {
  return a * b;
};

// You can also return null or undefined, but we usally don't give this function return statement
const logger = (message: string): void => {
  console.log(message);
};
```

Notice that we have to add types for arguments of function, there are no type inference for arguments. Type inference only works out output, **but we will ***NOT*** use it**. For example, if we forgot to return value for `add()` method. TS will infer that this function is no return. But that's not correct, we should return number in this case.

```ts
// TS thinks fakeAdd will return void so there's no warning
const fakeAdd = (a: number, b: number) => {
  a + b;
};
```

Sometimes our function needs to handle error, because functions will never return thrown error, so we can ignore it like so:

```ts
const throwError = (message: string): string => {
  if (!message) {
    throw new Error(message);
  }
  return message;
};
```
How about the function always throws error? In this special case, we can use special type `never` or just use `void`:

```ts
const throwError2 = (message: string): never => {
  throw new Error(message);
};
```

For destructing in ES2015:

```ts
const todaysWeather = {
  date: new Date(),
  weather: 'sunny',
};

// Destructing with annotations
const logWeather = ({
  date,
  weather,
}: {
  date: Date;
  weather: string;
}): void => {
  console.log(date);
  console.log(weather);
};

logWeather(todaysWeather);
```

### Type Annotations for Objects
The concept is same with above 2 sections. Let's see a complex example for destructing (see [objects.ts](features/annotations/objects.ts)):
```ts
const profile = {
  name: 'alex',
  age: 20,
  coords: {
    lat: 0,
    lng: 15,
  },
  setAge(age: number): void {
    this.age = age;
  },
};

// Destructing with annotations
const { age, name }: { age: number; name: string } = profile;
const {
  coords: { lat, lng },
}: { coords: { lat: number; lng: number } } = profile;
```

## Types Arrays

Types arrays is arrays where each elements is some consistent type of value. See examples in [arrays.ts](features/arrays.ts).
- TS can do type inference when extracting values from an array.
- TS can prevent us from adding incompatible values to the array.
- We can get help with `map`, `forEach`, `reduce` functions.
    ```ts
    const carMakers = ['ford', 'toyota', 'chevy'];
    carMakers.map((car: string): string => {
        return car.toUpperCase();
    });
    ```
- Arrays can still contain multiple different types.
    ```ts
    const importantDates: (Date | string)[] = [new Date()];
    importantDates.push('2030-10-10');
    importantDates.push(new Date());
    ```

We can use typed arrays any time we need to represent a collection of records with some arbitrary sort order.

### Tuples
*Tuple* is array-like structure where each element represents some property of a record. See the following image, we can put values in this object into an array-like stuff, this is so-called tuple. The order in tuple is important, each position represents different property with type.
<br/><img src="./screenshots/tuple.png" width="500"/><br/>

See following examples in [tuples.ts](features/tuples.ts):
```ts
const drink = {
  color: 'brown',
  carbonated: true,
  sugar: 40,
};

const pepsi: [string, boolean, number] = ['brown', true, 40];
```

We can also use *type alias* (we will introduce later) in above example.
```ts
type Drink = [string, boolean, number];
const pepsi: Drink = ['brown', true, 40];
```

> ### *Note*
> We don't use tuples often, because we lose the labels of data. So we don't know the meaning of the data from tuples.

## Interfaces

We can create a new type to describe the property names and values of an object, this new type we called *interface*. See the following examples in [interfaces.ts](features/interfaces.ts). We have to use long type annotation for this function, and it's not reusable.

```ts
const printVehicle = (vehicle: name: string; year: number; broken: boolean): void => {
  console.log(`Name: ${vehicle.name}`);
  console.log(`Year: ${vehicle.year}`);
  console.log(`Broken? ${vehicle.broken}`);
};
```

The solution is to create an interface to reuse this type. Notice that we always use upper case for naming of interfaces:
```ts
interface Vehicle {
  name: string;
  year: Date;
  broken: boolean;
}

const printVehicle = (vehicle: Vehicle): void => {
  console.log(`Name: ${vehicle.name}`);
  console.log(`Year: ${vehicle.year}`);
  console.log(`Broken? ${vehicle.broken}`);
};
```

You can also define a function in interface.
```ts
interface Vehicle {
  name: string;
  summary(): string;
}

const oldCivic = {
  name: 'civic',
  summary(): string {
    return `Name: ${this.name}`;
  }
};

const printVehicle = (vehicle: Vehicle): void => {
  console.log(vehicle.summary());
};
```

Now take a look for above example, if we don't use `name` property in `printVehicle`, we can delete it from interface. There is no error here. But the question is if `Vehicle` interface only have a function called `summary()`, it can represent vehicle? Of course not. Same issue happens in `printVehicle`, using `printSummary` might be more reasonable. The following code is after refactoring:

```ts
interface Reportable {
  summary(): string;
}

const printSummary = (vehicle: Reportable): void => {
  console.log(vehicle.summary());
};
```

If we have another object called `drink`, it also has (*implements*) `summary()` function. Can we use `printSummary` on it to print something? Of course. Any object which implements all properties and functions in this interface can use this interface, interface is reusable.

The general strategy for reusable code in TS:
- Create functions that accept arguments that are typed with interfaces.
- Objects or classes can decided to *implement* a given interface to work with a function.

## Classes

Class is a blueprint to create an object with some fields (values) and methods (functions) to represent a *thing*.

### Methods in Classes
You can use `extends` keyword to inherit other class and override some methods. Let's see some examples for methods in classes like so (see [classes.ts](features/classes.ts)):
```ts
class Vehicle {
  drive(): void {
    console.log('chugga chugga');
  }

  honk(): void {
    console.log('beep');
  }
}

// Car class has all properties and functions in Vehicle
class Car extends Vehicle {
  drive(): void {
    // Override
    console.log('vroom');
  }
}

const car = new Car();
car.drive();  // vroom
car.honk();   // beep
```

For methods in class, there are 3 different modifiers you can use:
- `public` (default): This method can be called any where, any time.
- `private`: This method can only be called by other methods in this class.
- `protected`: This method can be called by other methods in this class, or by other methods in child classes.

Notice that we cannot change modifier in child class. So if `drive()` in `Vehicle` is `public`, you cannot override it as `private drive()` in `Car`. The only way to set up `private` in this case, it's to remove this method from `Vehicle` class.

Another important question is when to use `private` modifier? We ***DO NOT*** mark methods as being private over any type of security concern, because we are not adding in any layer of application security. The only reason is **we don't want other developers to call**. This type of methods that might very deeply manipulate a class and we don't trust other developers to call that method safely.

### Fields in Classes
We can initialize field at first time we define it, or we can initialize it in `constructor()` method. `constructor()` is a special method, it will be called when you create an instance of this class. Let's see some examples for fields in class.
```ts
class Vehicle {
  color: string;

  constructor(color: string) {
    this.color = color;
  }
}

const vehicle = new Vehicle('orange');
console.log(vehicle.color); // orange
```

We can use short cut in constructor. The following `Vehicle` class is the same with above one.
```ts
class Vehicle {
  constructor(public color: string) {}
}
```

Fields can be inherited, see the following example. Notice that don't put `public` modifier before `color`, it will create a field called `color` for `Car` class too. But `color` should be belong to `Vehicle`.
```ts
class Vehicle {
  constructor(public color: string) {}

  protected honk(): void {
    console.log('beep');
  }
}

class Car extends Vehicle {
  // Don't put public modifier before color, it will create a field for Car
  constructor(public wheels: number, color: string) {
    super(color);
  }

  private drive(): void {
    console.log('vroom');
  }

  startDrivingProcess(): void {
    this.drive();
    this.honk();
  }
}

const car = new Car(4, 'red');
car.startDrivingProcess();
```

Class can be used to create an instance or refer to the type, one of the other. See following example:
```ts
// User is a class
import { User } from './User';

class App {
  // You can use Class to refer to the type
  signIn(user: User): string {
    ...
  }
}
```