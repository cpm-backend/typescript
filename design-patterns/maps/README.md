# Maps App

We will use Google API to get map information. You have to generate a Google project at [here](http://console.developers.google.com). After enabling Google Maps support inside the project, you can generate an API key. Then add this key in Google Maps script tag [here](index.html). Replace `<YOUR_KEY>` with your API key. For example:
```html
<script src="https://maps.googleapis.com/maps/api/js?key=abcde12345"></script>
```