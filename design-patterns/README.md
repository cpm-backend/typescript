# Design Patterns with Typescript

In this section, we are going to use design pattern with *Typescript* (TS) to build some interesting apps.

First thing to do is to install a tool [Parcel](https://parceljs.org/getting_started.html) to help us run TS in the browser.
```
npm install -g parcel-bundler
```

We cannot run TS code inside the browser, *Parcel* will compile it to JS and load into the browser to replace the script tag (e.g. `<script src="./src/index.ts"></script>`).

## Maps App

See mockup of this app as follows:
<br/><img src="./screenshots/maps.png" width="600"/><br/>

You can refer to the [maps](maps/) project to learn following concepts. Follow the steps:
1. Go to [Google APIs](http://console.developers.google.com) and register an account.
1. Generate a Google dev project.
    <br/><img src="./screenshots/google-project.png" width="1050"/>
1. Select **Library** on the left panel and search `maps javascript api` then click on it.
1. Hit **Enable**.
1. Select **Credentials** on the left panel, create a credential with API key.
1. Go to [index.html](maps/index.html) and replace `<YOUR_KEY>` with your API key.
1. Install the dependencies.
    ```
    cd maps
    npm install
    ```
1. Run *Parcel* command to start service:
    ```
    parcel index.html
    ```
1. Open your browser and visit http://localhost:1234 (1234 is *Parcel* default port).

### Type Definition Files
We use capital letter for the file which is exported as `class`, e.g. [User.ts](maps/src/User.ts).

We use [faker](https://www.npmjs.com/package/faker) to generate some fake data randomly. When you use this library in your code, you might notice the following message:
<br/><img src="./screenshots/faker.png" width="500"/><br/>

That's because TS code doesn't figure out the JS without types. So we have to provide *type definition files* for TS. Some libraries have this files by default, e.g. `axios`. But sometimes it doesn't provide this file like above case, we have to install it manually.

We can find most of these files in the project [Definitely Typed](https://github.com/DefinitelyTyped/DefinitelyTyped). We can use following command to install:
```
npm install @types/<LIBRARY_NAME>
```

To resolve this issue, we can install type definition files for `faker` by `npm install @types/faker`.

The name of these definition files always end with `.d.ts`, they provide the type information for you. They are like document, you can find properties, methods, and functions in there. Try to read them.

### Export
Convention inside of TS is to ***NEVER*** use `default export` statement. The reason for that is then we don't have to worry about whether we have to include those curly braces (if we use `default export`, we cannot use curly braces). But this rule is not apply to third party modules. So NPM modules we use might still have a default export statement (e.g. `import faker from 'faker';`) and that's totally fine.

### Global Variables
In setup step, we use Google API key to handle map. Try to type `google` in your browser console then see some content. But in your code, TS doesn't understand that there is a global variable available inside of the project. That's why we install the following type definition file:
```
@types/googlemaps
```

Let's explore this type definition file (`node_modules/@types/googlemaps/reference/map.d.ts`). To read this file easily, you can type `command + shift + P` then type `folder level 2`, click enter. Try to read this file and figure out how to use those APIs.

### Hiding Functionality
After installing `googlemaps` package, you find a lot of methods you can use. But these methods may break our application if other developers call it inappropriately. The good practice is to hide `googlemaps` package from other developers to prevent they call it directly.

We use [CustomMap.ts](maps/src/CustomMap.ts) to hide `googlemaps` package from others. All they can in [index.ts](maps/src/index.ts) is to call `CustomMap` class.

### Design Pattern
To prevent duplicate code, we can create a function like so:
```ts
import { User } from './User';
import { Company } from './Company';

export class CustomMap {
  // Works but not perfect!!
  addMarker(mappable: User | Company): void {...}
}
```

It works for reuse, but not perfect. Because we might use more different classes as input in the future. Every time we want to add a different class, we have to add a new type for `mappable` in `addMarker()` like:
```ts
addMarker(mappable: User | Company | Activity | Station): void {...}
```

The best solution is to use *interface*. Every other class implements all methods and fields in this interface, they can be used like this interface. So we refactor our code like so:
```ts
// Instructions to every other class on how they can be an argumant to 'addMarker'
interface Mappable {
  location: {
    lat: number;
    lng: number;
  };
}

export class CustomMap {
  addMarker(mappable: Mappable): void {...}
}
```

If you want to make sure class implements specific interface, you can use keyword `implement`. This is totally optional, it helps you find error earlier.
```ts
// Export this interface
export interface Mappable {...}
```

```ts
import { Mappable } from './CustomMap';

// (Optional) Make sure User implements Mappable
export class User implements Mappable {...}
```

Let's check final result:
<br/><img src="./screenshots/maps-app.png" width="650"/><br/>

## Sort App

We are going to build an app to sort numbers, string and linked list. Let's see the mockup:
<br/><img src="./screenshots/sort.png" width="500"/><br/>

### Setup
If we use `tsc index.ts` directly, it will generate JS files in the same directory. We want to put the generated files into other directory, we can use TS config. Generate this file using following command:
```
tsc --init
```

In this file, we can uncomment 2 line as below and set up the paths like so:
```json
{
  "compilerOptions": {
    "outDir": "./build",
    "rootDir": "./src",
  }
}
```

Now try to use command `tsc` to compile, then you can find the compiled files are put under `build/` folder. If you want to compile automatically after change, you can use *watch mode* by following command:
```
tsc -w
```

But it's still not very convenient because we have to use another terminal to start our app by `node build/index.js`. So we can use `nodemon` and `currently` package to handle this issue. `concurrently` helps us run multiple scripts in the same time.
```
npm install nodemon concurrently
```

We add 3 scripts into [package.json](sort/package.json). `concurrently npm:start:*` means run all scripts that starts with `start:`.
```json
"scripts": {
  "start:build": "tsc -w",
  "start:run": "nodemon build/index.js",
  "start": "concurrently npm:start:*"
}
```
Now we can run `npm start`. Try to change the file, you should see updates immediately.

You can refer to the [sort](sort/) project to learn following concepts.

### Algorithm
We use *bubble sort* in this case. This is not a best algorithm to solve this question, but our point is on design pattern. We can create bubble sort very easy for array of numbers, but it seems not to apply on string. There are 2 issues here:
1. String is immutable. Yes we can turn string into an array of characters, but we cannot change original string.
    ```js
    const color = 'red'
    color[0] = 'B'
    console.log(color)  // red
    ```
1. JS uses ASCII to compare 2 strings. The result is not the same with what we thought. For example, there are 2 characters `X` and `a`, if we want to use *alphabetical order*, we expect `X` is at the end. But the truth is `X` is smaller than `a`. Type following code to check:
    ```js
    'X'.charCodeAt(0) // 88
    'a'.charCodeAt(0) // 97
    ```

For the second issue, we might use `if` statement to create a new logic for string in the same method. For the first question, we can try to use union operator (`|`) to allow more types in this argument:
```ts
class Sorter {
  // Union operator '|'
  constructor(public collection: number[] | string) {}

  sort(): void {
    const { length } = this.collection;

    for (let i = 0; i < length; i++) {
      for (let j = 0; j < length - i - 1; j++) {
        if (this.collection[j] > this.collection[j + 1]) {
          const temp = this.collection[j];
          // Warning message:
          // Index signature in type 'string | number[]' only permits reading
          this.collection[j] = this.collection[j + 1];
          this.collection[j + 1] = temp;
        }
      }
    }
    // If collection is a string, do this logic intead
    ...
  }
}
```

You will get warning message, because when we want to use string type in this logic, we cannot assign value to string. So our goal is to make sure only `number[]` can get into this logic. That's what we call *type guards*.

### Type Guards
We can use *type guards* to make sure some type to get into specific code block. There are 2 type guards:
- `typeof`: It can narrow type of a value to a primitive type. There are 4 types can use it: `number`, `string`, `boolean`, `symbol`. Notice that **even though `typeof` technically works for arrays and objects inside of JS as well, it does not function as a type guard in TS.**
- `instanceof`: It can narrow down every other type of value.

Let's see an example:
```ts
// If collection is an array of numbers
if (this.collection instanceof Array) {...}

// If collection is a string
if (typeof this.collection === 'string') {...}
```

Now it seems that we can solve both issues, but this is not ideal at all. If we want to add a new type in this function, we have to allow more types in arguments, and add a new `if` statement and type guard for it. That's not good.

### Design Pattern
So our strategy is to use interface. We can find the logic is different in different types, so we can extract logic to their own class. Then we use methods in interface to call them. See following image:
<br/><img src="./screenshots/sort-interface.png" width="400"/><br/>

So we can create an interface called `Sortable` like so:
```ts
interface Sortable {
  length: number;
  compare(leftIndex: number, rightIndex: number): boolean;
  swap(leftIndex: number, rightIndex: number): void;
}
```

`Sorter` class uses methods in `Sortable` interface. So we can make sure all classes that implements `Sortable` then it can be called by `Sorter` correctly. In this way, we don't need to update `Sorter` if new type is added. All we need to do is to implement the `Sortable` interface when adding this new type.

Notice that `length` is a field, but we define it in [NumbersCollection](sort/src/NumbersCollection.ts) like a method. When we add a keyword `get` before this method, we define it as a getter. Then we can call it like calling a field.
```ts
export class NumbersCollection {
  constructor(public data: number[]) {}

  // Using 'get' we can call this method like a field
  get length(): number {
    return this.data.length;
  }
}

const collection = new NumbersCollection([1, 2, 3]);
console.log(collection.length);  // 3
```

Finally we can sort different types of data like so:
```ts
export class Sorter {
  constructor(public collection: Sortable) {}

  // Bubble sort
  sort(): void {
    const { length } = this.collection;

    for (let i = 0; i < length; i++) {
      for (let j = 0; j < length - i - 1; j++) {
        if (this.collection.compare(j, j + 1)) {
          this.collection.swap(j, j + 1);
        }
      }
    }
  }
}
```
```ts
// index.ts

// number[]
const numbersCollection = new NumbersCollection([10, 3, -5, 0]);
const numberSorter = new Sorter(numbersCollection);
numberSorter.sort();
console.log(numbersCollection.data);

// string
const charactersCollection = new CharactersCollection('Xaayb');
const stringSorter = new Sorter(charactersCollection);
stringSorter.sort();
console.log(charactersCollection.data);

// linked list
const linkedList = new LinkedList();
linkedList.add(500);
linkedList.add(-10);
linkedList.add(-3);
linkedList.add(4);
const linkedListSorter = new Sorter(linkedList);
linkedListSorter.sort();
linkedList.print();
```

There is still a problem here. If we want to sort something, we have to create an instance of `Sorter` class to sort. It's not ideal.

### Inheritance
Inheritance might help us solve above problem. Because inheritance takes all the methods from parent class and add them to child classes. 

For example, `NumbersCollection` extends `Sorter` then it has `sort()` method. And we remove the `constructor()` from `Sorter` because we won't create any instance of `Sorter`, we will call `swap()` and `compare()` directly in `NumbersCollection` class. But now we got problems:
```ts
export class Sorter {
  sort(): void {
    // Property 'length' does not exist
    const { length } = this;
    for (let i = 0; i < length; i++) {
      for (let j = 0; j < length - i - 1; j++) {
        // Property 'compare' does not exist
        if (this.compare(j, j + 1)) {
          // Property 'swap' does not exist
          this.swap(j, j + 1);
        }
      }
    }
  }
}
```

Because we plan to call those stuffs (`length`, `compare()` and `swap()`) in child class, so we should not define them in parent class. Unfortunately, TS does not know then it complains.

### Abstract Classes
To fix above issue, we can use abstract classes. Here are some features of abstract classes:
- It cannot be used to create an object directly.
- It only be used as a parent class.
- It can contain real implementation for some methods.
- The implemented methods can refer to other methods that don't actually exist yet.
- It can make child classes promise to implement some other methods (that don't exist in abstract classes).

See the final version of our `Sorter` abstract class. Notice that we don't need `Sortable` interface in this solution.
```ts
export abstract class Sorter {
  // Use 'abstract' to tell TS it will be implemented by some child class
  abstract compare(leftIndex: number, rightIndex: number): boolean;
  abstract swap(leftIndex: number, rightIndex: number): void;
  abstract length: number;

  // Bubble sort
  sort(): void {
    const { length } = this;

    for (let i = 0; i < length; i++) {
      for (let j = 0; j < length - i - 1; j++) {
        if (this.compare(j, j + 1)) {
          this.swap(j, j + 1);
        }
      }
    }
  }
}
```
For [NumbersCollection](sort/src/NumbersCollection.ts) class, we have to extend `Sorter` class and add `super()` in `constructor()`. When we use inheritance, we have to call parent's constructor first.
```ts
export class NumbersCollection extends Sorter {
  constructor(public data: number[]) {
    super();
  }
  ...
}
```
Check final version of [index.ts](sort/src/index.ts):
```ts
const numbersCollection = new NumbersCollection([10, 3, -5, 0]);
numbersCollection.sort();
console.log(numbersCollection.data);

const charactersCollection = new CharactersCollection('Xaayb');
charactersCollection.sort();
console.log(charactersCollection.data);

const linkedList = new LinkedList();
linkedList.add(500);
linkedList.add(-1);
linkedList.add(-3);
linkedList.add(4);
linkedList.sort();
linkedList.print();
```

### Interfaces vs Abstract Classes

Let's compare interfaces and abstract classes:

Interfaces|Inheritance/Abstract Classes
--|--
Sets up a contract between different classes.|Sets up a contract between different classes.
Use when we have very different objects that we want to work together.|Use when we are trying to build up a definition of an object.
Promotes loose coupling.|Strongly couples classes together.

In general, we always want to first reach for interfaces as a solution to code reuse unless we are in a scenario where we have some different objects that are very closely related.

## Stats App

We are going to build a stats app to generate a report for football game. Our source is this CSV [file](stats/football.csv). This app will load, parse, analyze it, then generate a report like so:
<br/><img src="./screenshots/stats.png" width="450"/><br/>

You can refer to the [stats](stats/) project to learn following concepts. Follow the steps:
1. Install the dependencies.
    ```
    cd sort
    npm install
    ```
1. Start the service and result is in the terminal console.
    ```
    npm start
    ```

### First Take
We use `fs` library to load our CSV file. But we have to install a type definition file first. For all libraries built in Node JS, we can find type definition files in `@types/node`.

Let's do some stats. For example, we want to generate a report for "how many times does *Man United* win?":
```ts
import fs from 'fs';

// Load CSV file
const matches = fs
  .readFileSync('football.csv', {
    encoding: 'utf-8',
  })
  // Parse data
  .split('\n')
  .map((row: string): string[] => {
    return row.split(',');
  });

let manUnitedWins = 0;

// Analyze
for (let match of matches) {
  if (match[1] === 'Man United' && match[5] === 'H') {
    manUnitedWins++;
  } else if (match[2] === 'Man United' && match[5] === 'A') {
    manUnitedWins++;
  }
}

// Report
console.log(`Man United won ${manUnitedWins} games`);
```

But the above example is bad code. There are several places we can improve:
- There are magic string comparisons. e.g. `match[5] === 'H'`
- Source of data is hardcoded. If we put CSV reading logic in this file, it's hard to reuse in the future.
- Data array is all strings, even though it might have numbers in it.
- Variables named after a specific team. e.g. `manUnitedWins`.
- Analysis type is fixed.
- No ability to output the report in different formats.

### Enums
Enum short for enumeration is an object that stores some very closely related values. These values are always going to be either numbers or strings. When we create an enum, we create this type as well. For example:
```ts
enum MatchResult {
  HomeWin = 'H',
  AwayWin = 'A',
  Draw = 'D',
}
```

Enum has some features:
- It follows near-identical syntax rules as normal objects.
- It creates an object with the same keys and values when converted from TS to JS.
- Primary goal is to signal to other engineers that these are all closely related values.
- Use whenever we have a small fixed set of values that are all closely related and known at compile time.

It's possible to define an enum without any values inside of it.
```ts
enum size {
  small,
  medium,
  big
}
```

If we want to turn string to enum type, this is called *type assertion*.
```ts
typeAssertion(row: string[]): MatchResult {
  return row[5] as MatchResult;
}
```

### Describing an Array with a Tuple
In an array, the different position with different type, it's good change to use tuple.
```ts
type MatchData = [Date, string, string, number, number, MatchResult, string];

export class CsvFileReader {
  data: MatchData[] = [];

  constructor(public fileName: string) {}

  // Load CSV file
  read(): void {
    this.data = fs
      .readFileSync(...)
      // Parse data
      ...
      .map(
        (row: string[]): MatchData => {
          return [
            dateStringToDate(row[0]),
            row[1],
            row[2],
            parseInt(row[3]),
            parseInt(row[4]),
            // Type assertion
            row[5] as MatchResult,
            row[6],
          ];
        }
      );
  }
}
```

### Generics
Generic is like function arguments, but for types in class or function definitions. It allows us to define the type of a property, argument or return value at a future point. See following example:
```ts
class HoldAnyThing<T> {
  data: T;
}

const holdNumber = new HoldAnyThing<number>();
holdNumber.data = 123;

const holdString = new HoldAnyThing<string>();
holdNumber.data = 'generics';
```

### Inheritance vs Composition
For above class, we have 2 strategies to refactor the code.
- Inheritance: It's characterized by an **is-a** relationship between 2 classes.
- Composition: It's characterized by an **has-a** relationship between 2 classes.

See following images to compare 2 designs:
<br/><img src="./screenshots/inheritance.png" width="550"/>
<br/><img src="./screenshots/composition.png" width="550"/><br/>

We prefer to use composition, if you want to check the implementation for inheritance, go to [src/inheritance](stats/src/inheritance/) folder. If you want to run it, please update the following script in [package.json](stats/package.json).
```json
"scripts": {
  "start:run": "nodemon build/inheritance/index.js",
}
```

> ### *Note*
> Favor object composition over class inheritance.

### Static Methods
We can create a *static* method to prevent creating instance of class every time. Static methods are belong to a class instead of an object. See following example, if we use `static` keyword on method, then this method is belong to class. We don't need to create an instance when we want to call it.
```ts
const matchReader = MatchReader.fromCsv('football.csv');
```
```ts
export class MatchReader {
  static fromCsv(fileName: string): MatchReader {
    return new MatchReader(new CsvFileReader(fileName));
  }
}
```