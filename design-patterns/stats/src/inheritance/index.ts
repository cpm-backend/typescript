import { MatchReader } from './MatchReader';
import { MatchResult } from './MatchResult';

const matchReader = new MatchReader('football.csv');
matchReader.read();

let manUnitedWins = 0;

// Analyze
for (let match of matchReader.data) {
  if (match[1] === 'Man United' && match[5] === MatchResult.HomeWin) {
    manUnitedWins++;
  } else if (match[2] === 'Man United' && match[5] === MatchResult.AwayWin) {
    manUnitedWins++;
  }
}

// Report
console.log(`Man United won ${manUnitedWins} games`);
