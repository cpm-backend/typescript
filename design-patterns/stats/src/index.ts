import { MatchReader } from './MatchReader';
import { WinsAnalysis } from './analyzers/WinsAnalysis';
import { Summary } from './Summary';
import { HtmlReport } from './reportTargets/HtmlReport';

// Using static methods
const matchReader = MatchReader.fromCsv('football.csv');
const summaryShowInConsole = Summary.winsAnalysisWithConsoleReport(
  'Man United'
);

const summaryShowInHtml = new Summary(
  new WinsAnalysis('Man United'),
  new HtmlReport()
);

matchReader.load();

summaryShowInHtml.buildAndPrintReport(matchReader.matches);
summaryShowInConsole.buildAndPrintReport(matchReader.matches);
