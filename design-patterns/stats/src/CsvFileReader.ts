import fs from 'fs';

export class CsvFileReader {
  data: string[][] = [];

  constructor(public fileName: string) {}

  // Load CSV file
  read(): void {
    this.data = fs
      .readFileSync(this.fileName, {
        encoding: 'utf-8',
      })
      // Parse data
      .split('\n')
      .map((row: string): string[] => {
        return row.split(',');
      });
  }
}
