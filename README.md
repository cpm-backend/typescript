# Typescript

This is a study note for learning *Typescript*.

There are 2 parts of this note:
1. [Basics](basics/): It's about syntax and features of *Typescript*.
1. [Design pattern](design-patterns/): We will learn design patterns with *Typescript* by projects.
    - [Maps](design-patterns/maps/): It can show fake user and company on the map. We will learn how to reuse the code by interface.
    - [Sort](design-patterns/sort/): We will build an app to sort numbers and characters. We will learn how to use abstract class.
    - [Stats](design-patterns/stats/): We will learn how  to load data and generate a report in this app. In this case, we will refactor the code by inheritance and composition.

## Reference

[Typescript: The Complete Developer's Guide](https://www.udemy.com/course/typescript-the-complete-developers-guide/).